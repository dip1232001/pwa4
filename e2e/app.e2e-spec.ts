import { Pwa4Page } from './app.po';

describe('pwa4 App', () => {
  let page: Pwa4Page;

  beforeEach(() => {
    page = new Pwa4Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
