import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MdSidenavModule} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DefaultComponent } from './default/default.component';
import { WebsitesComponent } from './websites/websites.component';
import { HeaderComponent } from './header/header.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
@NgModule({
  declarations: [
    AppComponent,
    DefaultComponent,
    WebsitesComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'pwa4'}),
    AppRoutingModule,
    BrowserAnimationsModule,
    MdSidenavModule
  ],
  exports: [
    MdSidenavModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
